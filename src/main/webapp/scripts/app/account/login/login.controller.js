'use strict';

angular.module('pinappleApp')
    .controller('LoginController', function ($rootScope, $scope, $state, $timeout, Auth) {
        $scope.user = {};
        $scope.errors = {};
        console.log("inside LoginController");
        $scope.rememberMe = true;
        $timeout(function (){angular.element('[ng-model="username"]').focus();});
        $scope.login = function (event) {
        	console.log("inside login function of LoginController");
            event.preventDefault();
            Auth.login({
                username: $scope.username,
                password: $scope.password,
                rememberMe: $scope.rememberMe
            }).then(function () {
            	console.log("inside login function of LoginController then success");
                $scope.authenticationError = false;
                if ($rootScope.previousStateName === 'register') {
                    $state.go('home');
                } else {
                    //$rootScope.back();
                	$state.go('home');
                }
            }).catch(function () {
                $scope.authenticationError = true;
            });
        };
    });
