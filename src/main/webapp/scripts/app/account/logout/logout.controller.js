'use strict';

angular.module('pinappleApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
