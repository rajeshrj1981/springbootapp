'use strict';

angular.module('pinappleApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('logout', {
                parent: 'site',
                url: '/logout',
                data: {
                    roles: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/main/main.html',
                        controller: 'LogoutController'
                    }
                }
            });
    });
