'use strict';

angular.module('pinappleApp')
    .controller('MainController', function ($scope, Principal) {
        var vmac = this;
        //$rootScope.vmac = vmac;
        $scope.vmac = vmac;
        vmac.gridApi;
        vmac.accrualsToolingColumns = getUiGridColumnsDefinitionAssetTooling();
        vmac.gridOptions = {
                enableRowSelection: true,
                enableSorting: true,
                enableRowHeaderSelection: false,
                columnDefs: vmac.accrualsToolingColumns,
                enableCellSelection: true,
                enableCellEditOnFocus: true,
                multiSelect: false,
                enableGridMenu: false,
                modifierKeysToMultiSelect: false,
                noUnselect: true,
                enableHorizontalScrollbar: 1,
                enableVerticalScrollbar: 1,
                rowHeight: 42,
                headerHeight: 36,
                showColumnMenu: true,
                resizable: true,
                expandableRowHeight: 0,
                enableExpandable: false,
                enableFiltering: true,
                enableColumnMenus: false,

                filterOptions: {
                    filterText: "",
                    useExternalFilter: false
                },
                onRegisterApi: handleGridRegisterAPI

            };
            vmac.gridOptions.data = [{stock: "TCS",
            						 quantity: 50,
                                     buyPrice: "INR",
                                     currentPrice: 0,
                                     investmentAmount: 400000,
                                     amountCurrency: "INR",
                                     purchaseDate: "01/03/2015"}
                                     ];
            
            function handleGridRegisterAPI(gridApi) {
                // Store the gridApi for later use.
                vmac.gridApi = gridApi;
                vmac.gridApi.core.on.rowsRendered($scope,function(){
                });
                // gridApi.selection.on.rowSelectionChanged($scope, handleGridRowSelectionChange);
            }
        
        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;
        });
    });


function getUiGridColumnsDefinitionAssetTooling() {

    var dateTemplate = '<div class="input-group ui-grid-cell-contents cell-with-input cell-input-off">'+
        '<input type="text" class="form-control cell-input" ng-click="grid.appScope.allowEdit($event);" ng-change="grid.appScope.assetLineChanged()" datepicker-popup="MM/dd/yyyy" ng-model="MODEL_COL_FIELD" is-open="opened" min-date="minDate" max-date="maxDate" ng-required="true" close-text="Close" />'+
        '<span class="input-group-btn">'+
        '<button type="button" class="btn btn-default tool-calender-btn" ng-click="grid.appScope.open($event)">'+
        '<i class="glyphicon glyphicon-calendar"></i>'+
        '</button>'+
        '</span>'+
        '</div>';

    function getDropDownTemplate(menus,field){
        return '<div class="input-group ui-grid-cell-contents cell-with-input cell-input-off"  dropdown="" keyboard-nav="" is-open="isopen">'+
            '<ul class="dropdown-menu" role="menu" aria-labelledby="simple-btn-keyboard-nav">'+
            '<li ng-repeat="d in grid.appScope.'+menus+'" role="menuitem"><a href="#" ng-click="grid.appScope.dropDownSelect(d,$event,\''+field+'\')">{{d}}</a></li>'+
            '</ul>'+
            '<input type="text" class="form-control cell-input" ng-model="MODEL_COL_FIELD" ng-click="grid.appScope.allowEdit($event)" />'+
            '<span class="input-group-btn">'+
            '<button type="button" class="btn btn-default custom-drop"  id="simple-btn-keyboard-nav" dropdown-toggle=""><div class="tooling-asset-dropdown-image"></div></button>'+
            '</span>'+
            '</div>';
    }

    return [

        {
            displayName: 'Stock', field: 'stock', width: '94', //pinnedLeft:true,
            cellClass: "tool-grid-cell",
            enableCellEdit: false,
            enableCellSelection: false,
            enableColumnResize: false,
            enableSorting: true,
            //pinnedLeft:true,
            //cellTemplate: '<div class="tool-grid-fav-col"><span id="{{row.entity.programId}}" ng-class="grid.appScope.checkFavProgram(row.entity)" ng-click="grid.appScope.makeProgramFavorite(row.entity)"></span></div>'
        },
        {
            displayName: 'Quantity', field: 'quantity', width: '200', //pinnedLeft:true,
            cellClass: "tool-grid-cell",
            visible: true,
            enableCellEdit: true,
            enableCellSelection: false,
            enableColumnResize: true
        },
        {
            displayName: 'Buy Price', field: 'buyPrice', width: '156',
            cellClass: "tool-grid-cell",
            enableCellEdit: false,
            visible: true
        },

        {
            displayName: 'Current Price', field: 'currentPrice', width: '156',
            cellClass: "tool-grid-cell",
            enableCellEdit: false,
            visible: true
        },

        {
            displayName: 'Investment Amount', field: 'investmentAmount', width: '156',
            enableCellEdit: false,
            cellClass: "tool-grid-cell",
            visible: true
            
            //cellTemplate: getDropDownTemplate('vmac.innerGridAccrualsDivisionsDropdown','glSiteId')
        },
        {
            displayName: 'Amount Currency', field: 'amountCurrency', width: '156',
            enableCellEdit: false,
            cellClass: "tool-grid-cell",
            visible: true
        },
        {
            displayName: 'Purchase Date', field: 'purchaseDate', width: '156',
            cellTemplate: dateTemplate,
            cellClass: "tool-grid-cell",
            enableCellEdit: false,
            visible: true
        }
    ];
};

