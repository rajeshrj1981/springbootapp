'use strict';

angular.module('pinappleApp')
    .factory('Principal', function Principal($q, Account) {
        var _identity,
            _authenticated = false;

        return {
            isIdentityResolved: function () {
                return angular.isDefined(_identity);
            },
            isAuthenticated: function () {
                return _authenticated;
            },
            isInRole: function (role) {
                if (!_authenticated) {
                   return false;
               }

               return this.identity().then(function(_id) {
            	   console.log("inside Principal factory this. identity() fuction");
                   return _id.roles && _id.roles.indexOf(role) !== -1;
               }, function(err){
                   return false;
               });
            },
            isInAnyRole: function (roles) {
                if (!_authenticated || !_identity || !_identity.roles) {
                    return false;
                }

                for (var i = 0; i < roles.length; i++) {
                    if (this.isInRole(roles[i])) {
                        return true;
                    }
                }

                return false;
            },
            authenticate: function (identity) {
                _identity = identity;
                _authenticated = identity !== null;
            },
            identity: function (force) {
            	console.log("inside Principal factory identity() fuction");
                var deferred = $q.defer();

                if (force === true) {
                    _identity = undefined;
                }

                // check and see if we have retrieved the identity data from the server.
                // if we have, reuse it by immediately resolving
                if (angular.isDefined(_identity)) {
                	console.log("inside Principal factory angular.isDefined fuction");
                    deferred.resolve(_identity);

                    return deferred.promise;
                }

                // retrieve the identity data from the server, update the identity object, and then resolve.
                Account.get().$promise
                    .then(function (account) {
                    	console.log("inside Principal factory Account.get()fuction"+account.data);
                        _identity = account.data;
                        _authenticated = true;
                        deferred.resolve(_identity);
                    })
                    .catch(function() {
                        _identity = null;
                        _authenticated = false;
                        deferred.resolve(_identity);
                    });
                return deferred.promise;
            }
        };
    });
